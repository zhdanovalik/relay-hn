const { GraphQLSchema } = require('graphql');

const { HackerNews } = require('graphqlhub-schemas');

module.exports = new GraphQLSchema({
    query: HackerNews.QueryObjectType
})