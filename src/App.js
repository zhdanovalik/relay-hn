import React, { Component } from 'react';
import { Link } from 'found'
import './App.css';
import { createFragmentContainer, graphql } from 'react-relay';
import User from './components/User'

class App extends Component {
    render() {
        return (
            <div>
                <header>Hello, how are ya ?</header>
                <nav>
                    <Link to="/">Home</Link>
                    <Link to="/about">About</Link>
                </nav>
                <User user={this.props.hn.user} />
                <div>
                    {this.props.children}
                </div>
            </div>
        );
    }
}

export default createFragmentContainer(
    App,
    graphql`
        fragment App_hn on HackerNewsAPI {
            user(id: $id) {
                ...User_user
            }
        }
    `
);
