import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import routes from './routes'

import HashProtocol from 'farce/lib/HashProtocol';
import queryMiddleware from 'farce/lib/queryMiddleware';
import createFarceRouter from 'found/lib/createFarceRouter';
import createRender from 'found/lib/createRender';

import { Resolver } from 'found-relay';
import { Environment, RecordSource, Store, Network } from 'relay-runtime';

import fetchQuery from './network'

const environment = new Environment({
    network: Network.create(fetchQuery),
    store: new Store(new RecordSource()),
});

const Router = createFarceRouter({
    historyProtocol: new HashProtocol(),
    historyMiddlewares: [queryMiddleware],
    routeConfig: routes,

    render: createRender({}),
});

const mountNode = document.createElement('div');
document.body.appendChild(mountNode);


ReactDOM.render(<Router resolver={new Resolver(environment)} />, mountNode);
