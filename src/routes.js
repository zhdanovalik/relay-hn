import { Route, makeRouteConfig } from 'found'
import React from 'react';
import { graphql } from 'react-relay';

import App from './App';
import Home from './components/Home';
import About from './components/About';

const TodoAppQuery = graphql`
    query routes_App_Query($id: String!) {
        hn {
            ...App_hn
        }
    }
`;

export default makeRouteConfig(
    <Route
        path="/"
        query={TodoAppQuery}
        prepareVariables={params => ({ ...params, id: 'tptacek' })}
        Component={App}>
        <Route Component={Home}/>
        <Route path=":about" Component={About} />
    </Route>,
);