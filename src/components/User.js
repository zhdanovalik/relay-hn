import React from 'react'
import { createFragmentContainer, graphql } from "react-relay";

const User = ({user}) => {
    return (
        <div>
            <div>User: {user.id}</div>
            <p><strong>About:</strong> <span>{user.about}</span></p>
        </div>
    )
};

export default createFragmentContainer(
    User,
    graphql`
        fragment User_user on HackerNewsUser {
            id,
            about,
            submitted {
                title
            }
        }
    `
);
