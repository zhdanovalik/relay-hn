import {
    HashProtocol,
    queryMiddleware
} from 'farce'
import {
    createFarceRouter,
    createRender
} from 'found'
import { Resolver } from 'found-relay';
import React from 'react';
import ReactDOM from 'react-dom';
import { Network } from 'relay-local-schema';
import { Environment, RecordSource, Store } from 'relay-runtime';

import { GraphQLSchema } from 'graphql'

import { HackerNews } from 'graphqlhub-schemas'

const schema = new GraphQLSchema({
    query: HackerNews.QueryObjectType
})

import routes from './routes';

const environment = new Environment({
    network: Network.create({ schema }),
    store: new Store(new RecordSource()),
});

const Router = createFarceRouter({
    historyProtocol: new HashProtocol(),
    historyMiddlewares: [queryMiddleware],
    routeConfig: routes,

    render: createRender({}),
});

const mountNode = document.createElement('div');
document.body.appendChild(mountNode);

ReactDOM.render(<Router resolver={new Resolver(environment)} />, mountNode);