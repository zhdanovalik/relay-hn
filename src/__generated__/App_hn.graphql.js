/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type {ConcreteFragment} from 'relay-runtime';
export type App_hn = {|
  +user: ?{| |};
|};
*/


const fragment /*: ConcreteFragment*/ = {
  "argumentDefinitions": [
    {
      "kind": "RootArgument",
      "name": "id",
      "type": "String!"
    }
  ],
  "kind": "Fragment",
  "metadata": null,
  "name": "App_hn",
  "selections": [
    {
      "kind": "LinkedField",
      "alias": null,
      "args": [
        {
          "kind": "Variable",
          "name": "id",
          "variableName": "id",
          "type": "String!"
        }
      ],
      "concreteType": "HackerNewsUser",
      "name": "user",
      "plural": false,
      "selections": [
        {
          "kind": "FragmentSpread",
          "name": "User_user",
          "args": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "HackerNewsAPI"
};

module.exports = fragment;
