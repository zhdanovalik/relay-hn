/**
 * @flow
 * @relayHash f1a89c666abc71a15d11375bd3b7a6ae
 */

/* eslint-disable */

'use strict';

/*::
import type {ConcreteBatch} from 'relay-runtime';
export type routes_App_QueryResponse = {|
  +hn: ?{| |};
|};
*/


/*
query routes_App_Query(
  $id: String!
) {
  hn {
    ...App_hn
  }
}

fragment App_hn on HackerNewsAPI {
  user(id: $id) {
    ...User_user
  }
}

fragment User_user on HackerNewsUser {
  id
  about
  submitted {
    title
  }
}
*/

const batch /*: ConcreteBatch*/ = {
  "fragment": {
    "argumentDefinitions": [
      {
        "kind": "LocalArgument",
        "name": "id",
        "type": "String!",
        "defaultValue": null
      }
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "routes_App_Query",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": null,
        "concreteType": "HackerNewsAPI",
        "name": "hn",
        "plural": false,
        "selections": [
          {
            "kind": "FragmentSpread",
            "name": "App_hn",
            "args": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "GraphqlHub"
  },
  "id": null,
  "kind": "Batch",
  "metadata": {},
  "name": "routes_App_Query",
  "query": {
    "argumentDefinitions": [
      {
        "kind": "LocalArgument",
        "name": "id",
        "type": "String!",
        "defaultValue": null
      }
    ],
    "kind": "Root",
    "name": "routes_App_Query",
    "operation": "query",
    "selections": [
      {
        "kind": "LinkedField",
        "alias": null,
        "args": null,
        "concreteType": "HackerNewsAPI",
        "name": "hn",
        "plural": false,
        "selections": [
          {
            "kind": "LinkedField",
            "alias": null,
            "args": [
              {
                "kind": "Variable",
                "name": "id",
                "variableName": "id",
                "type": "String!"
              }
            ],
            "concreteType": "HackerNewsUser",
            "name": "user",
            "plural": false,
            "selections": [
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "id",
                "storageKey": null
              },
              {
                "kind": "ScalarField",
                "alias": null,
                "args": null,
                "name": "about",
                "storageKey": null
              },
              {
                "kind": "LinkedField",
                "alias": null,
                "args": null,
                "concreteType": "HackerNewsItem",
                "name": "submitted",
                "plural": true,
                "selections": [
                  {
                    "kind": "ScalarField",
                    "alias": null,
                    "args": null,
                    "name": "title",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "text": "query routes_App_Query(\n  $id: String!\n) {\n  hn {\n    ...App_hn\n  }\n}\n\nfragment App_hn on HackerNewsAPI {\n  user(id: $id) {\n    ...User_user\n  }\n}\n\nfragment User_user on HackerNewsUser {\n  id\n  about\n  submitted {\n    title\n  }\n}\n"
};

module.exports = batch;
